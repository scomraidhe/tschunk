import datetime
import os
import re
import subprocess
import threading
import time
import sublime
import sublime_plugin
import sys

try:
    from HTMLParser import HTMLParser
except:
    from html.parser import HTMLParser
from os.path import expanduser

class Pref:

    project_file = None

    keys = [
        "show_debug",
        "extensions_to_execute",
        "extensions_to_blacklist",
        "Tschunk_execute_on_save",
        "Tschunk_show_errors_on_save",
        "Tschunk_show_gutter_marks",
        "Tschunk_outline_for_errors",
        "Tschunk_show_errors_in_status",
        "Tschunk_show_quick_panel",
        "Tschunk_php_prefix_path",
        "Tschunk_commands_to_php_prefix",
        "Tschunk_icon_scope_color",
        "Tschunk_sniffer_run",
        "Tschunk_command_on_save",
        "Tschunk_executable_path",
        "Tschunk_additional_args",
	"Tschunk_api_key"
    ]

    def load(self):
        self.settings = sublime.load_settings('tschunk.sublime-settings')

        if sublime.active_window() is not None:
            project_settings = sublime.active_window().active_view().settings()
            if project_settings.has("Tschunk"):
                project_settings.clear_on_change('Tschunk')
                self.project_settings = project_settings.get('Tschunk')
                project_settings.add_on_change('Tschunk', pref.load)
            else:
                self.project_settings = {}
        else:
            self.project_settings = {}

        for key in self.keys:
            self.settings.clear_on_change(key)
            setattr(self, key, self.get_setting(key))
            self.settings.add_on_change(key, pref.load)

    def get_setting(self, key):
        if key in self.project_settings:
            return self.project_settings.get(key)
        else:
            return self.settings.get(key)

    def set_setting(self, key, value):
        if key in self.project_settings:
            self.project_settings[key] = value
        else:
            self.settings.set(key, value)


pref = Pref()

st_version = 2
if sublime.version() == '' or int(sublime.version()) > 3000:
    st_version = 3

if st_version == 2:
    pref.load()

def plugin_loaded():
    pref.load()

def debug_message(msg):
    if pref.show_debug == True:
        print("[Tschunk] " + str(msg))


class CheckstyleError():
    """Represents an error that needs to be displayed on the UI for the user"""
    def __init__(self, line, message):
        self.line = line
        self.message = message

    def get_line(self):
        return self.line

    def get_message(self):
        data = self.message

        if st_version == 3:
            return HTMLParser().unescape(data)
        else:
            try:
                data = data.decode('utf-8')
            except UnicodeDecodeError:
                data = data.decode(sublime.active_window().active_view().settings().get('fallback_encoding'))
            return HTMLParser().unescape(data)

    def set_point(self, point):
        self.point = point

    def get_point(self):
        return self.point


class ShellCommand():
    """Base class for shelling out a command to the terminal"""
    def __init__(self):
        self.error_list = []

    def get_errors(self, path):
        self.execute(path)
        return self.error_list

    def shell_out(self, cmd):
        data = None

        if st_version == 3:
            debug_message(' '.join(cmd))
        else:
            for index, arg in enumerate(cmd[:]):
                cmd[index] = arg.encode(sys.getfilesystemencoding())

            debug_message(' '.join(cmd))

        debug_message(' '.join(cmd))
	#debug_message(cmd)

        info = None
        if os.name == 'nt':
            info = subprocess.STARTUPINFO()
            info.dwFlags |= subprocess.STARTF_USESHOWWINDOW
            info.wShowWindow = subprocess.SW_HIDE

        home = expanduser("~")
        debug_message("cwd: " + home)

        proc = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, startupinfo=info, cwd=home)


        if proc.stdout:
            data = proc.communicate()[0]

        if st_version == 3:
            return data.decode()
        else:
            return data

    def execute(self, path):
        debug_message('Command not implemented')


class Sniffer(ShellCommand):
    def execute(self, path):
        if pref.Tschunk_sniffer_run != True:
            return

        args = []

        if pref.Tschunk_php_prefix_path != "" and self.__class__.__name__ in pref.Tschunk_commands_to_php_prefix:
            args = [pref.Tschunk_php_prefix_path]

        if pref.Tschunk_executable_path != "":
            application_path = pref.Tschunk_executable_path
        else:
            application_path = 'Tschunk'

        if (len(args) > 0):
            args.append(application_path)
        else:
            args = [application_path]

        args.append("--report=checkstyle")

        # Add the additional arguments from the settings file to the command
        for key, value in pref.Tschunk_additional_args.items():
            arg = key
            if value != "":
                arg += "=" + value
            args.append(arg)

        #args.append(os.path.normpath(path))
        args = os.path.normpath(path)
	self.parse_report(args)

    def parse_report(self, args):
	#debug_message("SHOWING ARGS")
	#debug_message(args)
        if pref.Tschunk_api_key != None:
		api_key = pref.Tschunk_api_key
	else:
		api_key = "NONE"

	cmdargs = ["curl", "-k", "-s", "-f", "-X", "POST", "--data-binary", "@" + args, "https://user:pass@tschunk.ch0wn.org/webscan/perform.cgi?apikey=" + api_key]
        report = self.shell_out(cmdargs)
        debug_message(report)
        lines = re.finditer('.*line="(?P<line>\d+)" column="(?P<column>\d+)" severity="(?P<severity>\w+)" message="(?P<message>.*)" source', report)

        for line in lines:
            error = CheckstyleError(line.group('line'), line.group('message'))
            self.error_list.append(error)

    def get_standards_available(self):
        if pref.Tschunk_executable_path != "":
            application_path = pref.Tschunk_executable_path
        else:
            application_path = 'Tschunk'

        args = []
        args.append(application_path)
        output = self.shell_out(args)
        standards = output[35:].replace('and', ',').strip().split(', ')
        return standards

class TschunkCommand():
    """Main plugin class for building the checkstyle report"""

    # Class variable, stores the instances.
    instances = {}

    @staticmethod
    def instance(view, allow_new=True):
        '''Return the last-used instance for a given view.'''
        view_id = view.id()
        if view_id not in TschunkCommand.instances:
            if not allow_new:
                return False
            TschunkCommand.instances[view_id] = TschunkCommand(view)
        return TschunkCommand.instances[view_id]

    def __init__(self, view):
        self.view = view
        self.checkstyle_reports = []
        self.report = []
        self.event = None
        self.error_lines = {}
        self.error_list = []
        self.shell_commands = ['Sniffer']
        self.standards = []

    def run(self, path, event=None):
        self.event = event
        self.checkstyle_reports = []
        self.report = []

        if event != 'on_save':
            if pref.Tschunk_sniffer_run:
                self.checkstyle_reports.append(['Sniffer', Sniffer().get_errors(path), 'dot'])
        else:
            if pref.Tschunk_command_on_save and pref.Tschunk_sniffer_run:
                self.checkstyle_reports.append(['Sniffer', Sniffer().get_errors(path), 'dot'])

        sublime.set_timeout(self.generate, 0)

    def clear_sniffer_marks(self):
        for region in self.shell_commands:
            self.view.erase_regions(region)

    def set_status_bar(self):
        if not pref.Tschunk_show_errors_in_status:
            return

        if self.view.is_scratch():
            return

        line = self.view.rowcol(self.view.sel()[0].end())[0]
        errors = self.get_errors(line)
        if errors:
            self.view.set_status('Tschunk', errors)
        else:
            self.view.erase_status('Tschunk')

    def generate(self):
        self.error_list = []
        region_set = []
        self.error_lines = {}

        for shell_command, report, icon in self.checkstyle_reports:
            self.view.erase_regions('checkstyle')
            self.view.erase_regions(shell_command)

            debug_message(shell_command + ' found ' + str(len(report)) + ' errors')
            for error in report:
                line = int(error.get_line())
                pt = self.view.text_point(line - 1, 0)
                region_line = self.view.line(pt)
                region_set.append(region_line)
                self.error_list.append('(' + str(line) + ') ' + error.get_message())
                error.set_point(pt)
                self.report.append(error)
                self.error_lines[line] = error.get_message()

            if len(self.error_list) > 0:
                icon = icon if pref.Tschunk_show_gutter_marks else ''
                outline = sublime.DRAW_OUTLINED if pref.Tschunk_outline_for_errors else sublime.HIDDEN
                if pref.Tschunk_show_gutter_marks or pref.Tschunk_outline_for_errors:
                    if pref.Tschunk_icon_scope_color == None:
                        debug_message("WARN: Tschunk_icon_scope_color is not defined, so resorting to Tschunk colour scope")
                        pref.Tschunk_icon_scope_color = "Tschunk"
                    self.view.add_regions(shell_command, region_set, pref.Tschunk_icon_scope_color, icon, outline)

        if pref.Tschunk_show_quick_panel == True:
            # Skip showing the errors if we ran on save, and the option isn't set.
            if self.event == 'on_save' and not pref.Tschunk_show_errors_on_save:
                return
            self.show_quick_panel()

    def show_quick_panel(self):
        self.view.window().show_quick_panel(self.error_list, self.on_quick_panel_done)

    def fix_standards_errors(self, tool, path):
        self.error_lines = {}
        self.error_list = []
        self.report = []

        if tool == "CodeBeautifier":
            fixes = CodeBeautifier().get_errors(path)
        else:
            fixes = Fixer().get_errors(path)

        for fix in fixes:
            self.error_list.append(fix.get_message())

        if pref.Tschunk_fixer_show_quick_panel == True:
            self.show_quick_panel()

    def display_coding_standards(self):
        self.standards = Sniffer().get_standards_available()
        self.view.window().show_quick_panel(self.standards, self.on_coding_standard_change)

    def on_coding_standard_change(self, picked):
        if picked == -1:
            return

        current_additional_args = pref.get_setting('Tschunk_additional_args')
        current_additional_args['--standard'] = self.standards[picked].replace(' ', '')

        pref.set_setting('Tschunk_additional_args', current_additional_args)
        debug_message(current_additional_args)

    def on_quick_panel_done(self, picked):
        if picked == -1:
            return

        if (len(self.report) > 0):
            pt = self.report[picked].get_point()
            self.view.sel().clear()
            self.view.sel().add(sublime.Region(pt))
            self.view.show(pt)
            self.set_status_bar()

    def get_errors(self, line):
        if not line + 1 in self.error_lines:
            return False

        return self.error_lines[line + 1]

    def get_next_error(self, line):
        current_line = line + 1

        cache_error=None
        # todo: Need a way of getting the line count of the current file!
        cache_line=1000000
        for error in self.report:
            error_line = error.get_line()

            if cache_error != None:
                cache_line = cache_error.get_line()

            if int(error_line) > int(current_line) and int(error_line) < int(cache_line):
                cache_error = error

        if cache_error != None:
            pt = cache_error.get_point()
            self.view.sel().clear()
            self.view.sel().add(sublime.Region(pt))
            self.view.show(pt)


class TschunkTextBase(sublime_plugin.TextCommand):
    """Base class for Text commands in the plugin, mainly here to check php files"""
    description = ''

    def run(self, args):
        debug_message('Not implemented')

    def description(self):
        if not TschunkTextBase.should_execute(self.view):
            return "Invalid file format"
        else:
            return self.description

    @staticmethod
    def should_execute(view):
        if view.file_name() != None:

            try:
                ext = os.path.splitext(view.file_name())[1]
                result = ext[1:] in pref.extensions_to_execute
            except:
                debug_message("Is 'extensions_to_execute' setup correctly")
                return False

            for block in pref.extensions_to_blacklist:
                match = re.search(block, view.file_name())
                if match != None:
                    return False

            return result

        return False


class TschunkSniffThisFile(TschunkTextBase):
    """Command to sniff the open file"""
    description = 'Sniff this file...'

    def run(self, args):
        cmd = TschunkCommand.instance(self.view)
        cmd.run(self.view.file_name())

    def is_enabled(self):
        return TschunkTextBase.should_execute(self.view)


class TschunkShowPreviousErrors(TschunkTextBase):
    '''Command to show the previous sniff errors.'''
    description = 'Display sniff errors...'

    def run(self, args):
        cmd = TschunkCommand.instance(self.view, False)
        cmd.show_quick_panel()

    def is_enabled(self):
        '''This command is only enabled if it's a PHP buffer with previous errors.'''
        return TschunkTextBase.should_execute(self.view) \
            and TschunkCommand.instance(self.view, False) \
            and len(TschunkCommand.instance(self.view, False).error_list) > 0


class TschunkGotoNextErrorCommand(TschunkTextBase):
    """Go to the next error from the current position"""
    def run(self, args):
        line = self.view.rowcol(self.view.sel()[0].end())[0]

        cmd = TschunkCommand.instance(self.view)
        next_line = cmd.get_next_error(line)

    def is_enabled(self):
        '''This command is only enabled if it's a PHP buffer with previous errors.'''

        return TschunkTextBase.should_execute(self.view) \
            and TschunkCommand.instance(self.view, False) \
            and len(TschunkCommand.instance(self.view, False).error_list) > 0


class TschunkClearSnifferMarksCommand(TschunkTextBase):
    """Command to clear the sniffer marks from the view"""
    description = 'Clear sniffer marks...'

    def run(self, args):
        cmd = TschunkCommand.instance(self.view)
        cmd.clear_sniffer_marks()

    def is_enabled(self):
        return TschunkTextBase.should_execute(self.view)

class TschunkTogglePlugin(TschunkTextBase):
    """Command to toggle if plugin should execute on save"""
    def run(self, edit, toggle=None):
        if toggle == None:
            if pref.Tschunk_execute_on_save == True:
                pref.Tschunk_execute_on_save = False
            else:
                pref.Tschunk_execute_on_save = True
        else:
            if toggle :
                pref.Tschunk_execute_on_save = True
            else:
                pref.Tschunk_execute_on_save = False

    def is_enabled(self):
        return TschunkTextBase.should_execute(self.view)

    def description(self, paths=[]):
        if pref.Tschunk_execute_on_save == True:
            description = 'Turn Execute On Save Off'
        else:
            description = 'Turn Execute On Save On'
        return description

class TschunkSwitchCodingStandard(TschunkTextBase):
    """Ability to switch the coding standard for this session"""
    def run(self, args):
        cmd = TschunkCommand.instance(self.view)
        cmd.display_coding_standards()

    def is_enabled(self):
        return TschunkTextBase.should_execute(self.view)

class TschunkEventListener(sublime_plugin.EventListener):
    """Event listener for the plugin"""
    def on_post_save(self, view):
        if TschunkTextBase.should_execute(view):
            if pref.Tschunk_execute_on_save == True:
                cmd = TschunkCommand.instance(view)
                thread = threading.Thread(target=cmd.run, args=(view.file_name(), 'on_save'))
                thread.start()

            if pref.Tschunk_execute_on_save == True and pref.Tschunk_fixer_on_save == True:
                cmd = TschunkCommand.instance(view)
                cmd.fix_standards_errors("Fixer", view.file_name())

            if pref.Tschunk_execute_on_save == True and pref.phpcbf_on_save == True:
                cmd = TschunkCommand.instance(view)
                cmd.fix_standards_errors("CodeBeautifier", view.file_name())

    def on_selection_modified(self, view):
        if not TschunkTextBase.should_execute(view):
            return

        cmd = TschunkCommand.instance(view, False)
        if isinstance(cmd, TschunkCommand):
            cmd.set_status_bar()

    def on_pre_save(self, view):
        """ Project based settings, currently able to see an API based way of doing this! """
        if not TschunkTextBase.should_execute(view) or st_version == 2:
            return

        current_project_file = view.window().project_file_name();
        debug_message('Project files:')
        debug_message(' Current: ' + str(current_project_file))
        debug_message(' Last Known: ' + str(pref.project_file))

        if current_project_file == None:
            debug_message('No project file defined, therefore skipping reload')
            return

        if pref.project_file == current_project_file:
            debug_message('Project files are the same, skipping reload')
        else:
            debug_message('Project files have changed, commence the reload')
            pref.load();
            pref.project_file = current_project_file
